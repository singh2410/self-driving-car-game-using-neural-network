# Self driving car game using Neural Network

Implementation of Neural Networks in Self Driving Cars and getting insights of how actually mechanism of self driving works with Implementation.

## Project Name
Self-driving car game using Neural Network virtual Implementation.

## Description
Every year, traffic accidents account for 2.2% of global deaths. That stacks up to roughly 1.3 million a year — 3,287 a day. On top of this, some 20–50 million people are seriously injured in auto-related accidents each year. The root of these accidents? Human error.
From distracted driving to drunk driving to reckless driving to careless driving, one poor or inattentive decision could be the difference between a typical drive. But what if we could neutralize human error from the equation?
“Autonomous cars are no longer beholden to Hollywood sci-fi films” — Elon Musk, the founder of Tesla Inc. and SpaceX believes within a decade, self-driving cars will be as common as elevators.
Industry experts say the technology is going to disrupt and revolutionize the future of transportation as we know it.

## The Challenge
Train an end-to-end deep learning model that would let a car drive by itself around the track in a driving simulator. It is a supervised regression problem between the car steering angles and the road images in real-time from the cameras of a car.
This project has been inspired from the Nvidia’s end to end self driving car paper.

## Data Collection
[](https://miro.medium.com/max/1400/1*OeziRNGI1XspCf9RZCqyFA.png)
In this project, driving simulator has two different tracks. One of them was used for collecting training data, and the other one — never seen by the model — as a substitute for the test set.

The driving simulator would save frames from three front-facing “cameras”, recording data from the car’s point of view; as well as various driving statistics like throttle, speed, and steering angle. We are going to use camera data as model input and expect it to predict the steering angle in the [-1, 1] range.

## Environment and Tools
* matplotlib
* keras
* numpy
* pandas
* scikit-learn
* tensorflow

## Working
[](https://miro.medium.com/max/1400/1*OeziRNGI1XspCf9RZCqyFA.png)


## References
* [Nvidia](https://developer.nvidia.com/blog/deep-learning-self-driving-cars/)
* [Jae Duk Seo- Medium](https://towardsdatascience.com/implementing-neural-network-used-for-self-driving-cars-from-nvidia-with-interactive-code-manual-aa6780bc70f4)



## Authors and acknowledgment
Aarush Kumar

## License
Released under Apache 2.0 Open Source Community License.

## Thankyou!!!
